#!/bin/bash

# Terminate already running bar instances
pkill polybar

# Launch Polybar, using config location ~/.config/polybar/config
polybar -r main &

echo "Polybar launched..."
