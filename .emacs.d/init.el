;; import the configuration from the main Org config file
(org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("f17febd3c5f8ab732ee5c69b6e696fcaee9cc6e114cfb4993e102a2391e082b4" "6163008d6e4a4b5a1705e58796892168630d7f7eeb9fc5388f95b629b4468894" "446cc97923e30dec43f10573ac085e384975d8a0c55159464ea6ef001f4a16ba" default))
 '(package-selected-packages
   '(yaml-mode dockerfile-mode docker ein jupyter magit projectile counsel hydra general helpful doom-themes which-key use-package org-bullets ivy doom-modeline dashboard beacon base16-theme)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
