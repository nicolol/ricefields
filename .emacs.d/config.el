;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Startup][Startup:1]]

;; Startup:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Interface][Interface:1]]
;; inhibit the startup message
(setq inhibit-startup-message t)

;; remove the toolbar from the window
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)

;; disable the annoying bell
(setq ring-bell-function 'ignore)
;; but configure a visual ("optic") line bell
(setq visible-bell t)

;; convert yes/no to y/n in 
(fset 'yes-or-no-p 'y-or-n-p)
(global-set-key (kbd "<f5>") 'revert-buffer)
;; Interface:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Packages][Packages:1]]
;; Initialize package sources
(require 'package)

;; (setq package-enable-at-startup nil)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Load and initialize emacs lisp packages,
;; refresh said packages if that's the case.
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; Ensure use-package is running
(require 'use-package)
(setq use-package-always-ensure t)
;; Packages:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Theming][Theming:1]]
;; select the default theme
(use-package base16-theme :ensure t)
(load-theme 'base16-material)
;; (load-theme 'base16-oceanicnext)

;; set frame font
;; (set-frame-font "Fira Code 12") ; old way of setting the font
(set-face-attribute 'default nil :font "Fira Code Retina" :height 140)
;; Theming:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Which-key][Which-key:1]]
(use-package which-key
  :ensure t
  :config
  (which-key-mode))
;; Which-key:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Ivy/Counsel/Swiper][Ivy/Counsel/Swiper:1]]
;; ;; counsel settings
;; (use-package counsel
;;   :ensure t
;;   :bind
;;   (("M-y" . counsel-yank-pop)
;;    :map ivy-minibuffer-map
;;    ("M-y" . ivy-next-line)))

;; ;; ivy settings
;; (use-package ivy
;;   :ensure t
;;   :diminish (ivy-mode)
;;   :bind (("C-x b" . ivy-switch-buffer))
;;   :config (ivy-mode 1)
;;   (setq ivy-use-virtual-buffer t)
;;   (setq enable-recursive-minibuffers t)
;;   (setq ivy-count-format "%d%d ")
;;   (setq ivy-display-style 'fancy))

;; ;; swiper settings
;; (use-package swiper
;;   :ensure t
;;   :bind (("C-s" . swiper-isearch)
;; 	 ("C-r" . swiper-isearch)
;; 	 ("C-c C-r" . ivy-resume)
;; 	 ("M-x" . counsel-M-x)
;; 	 ("C-x C-f" . counsel-find-file)
;; 	 ("<f1> f" . 'counsel-describe-function)
;; 	 ("<f1> v" . 'counsel-describe-variable)
;; 	 ("<f1> l" . 'counsel-find-library)
;; 	 ("<f2> i" . 'counsel-info-lookup-symbol)
;; 	 ("<f2> u" . 'counsel-unicode-char)
;; 	 ("C-c g" . 'counsel-git)
;; 	 ("C-c j" . 'counsel-git-grep)
;; 	 ("C-c k" . 'counsel-ag)
;; 	 ("C-x l" . 'counsel-locate)
;; 	 ("C-S-o" . 'counsel-rhythmbox))

;;   :config
;;   (progn
;;     (ivy-mode 1)
;;     (setq ivy-use-virtual-buffers t)
;;     (setq ivy-display-style 'fancy)
;;     (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
;;     ))

;; ;; load the configuration for ivy
;; (setq ivy-use-virtual-buffers t)

;; ;; enable this if you want `swiper' to use it
;; (setq search-default-mode #'char-fold-to-regexp)
;; (global-set-key "\C-s" 'swiper)
;; (global-set-key (kbd "C-c C-r") 'ivy-resume)
;; (global-set-key (kbd "<f6>") 'ivy-resume)
;; (global-set-key (kbd "M-x") 'counsel-M-x)
;; (global-set-key (kbd "C-x C-f") 'counsel-find-file)
;; (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
;; Ivy/Counsel/Swiper:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Ivy/Counsel/Swiper][Ivy/Counsel/Swiper:2]]
;; Ivy setup
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)	
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))
;; Ivy/Counsel/Swiper:2 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Doom-modeline][Doom-modeline:1]]
(use-package doom-modeline
  :ensure 
  ;; :hook (after-init . doom-modeline-mode))
  :init (doom-modeline-mode 1))
;; Doom-modeline:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Dashboard][Dashboard:1]]
;; use the dashboard package
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

;; change the default logo
(setq dashboard-startup-banner 'logo)
;; Dashboard:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Elpy][Elpy:1]]
;; choose the default version of python
;; (setq py-python-command "python3")
;; (setq py-python-shell-interpreter "python3") ;

;; setup elpy
;; (use-package elpy
;;   :ensure t
;;   :bind(
;;         :map elpy-mode-map
;;          ("C-M-n" . elpy-nav-forward-block)
;;          ("C-M-p" . elpy-nav-backward-block))
;;   :hook(
;;         (elpy-mode .  flycheck-mode)
;;         (elpy-mode . (lambda ()
;;                        (set (make-local-variable 'company-backends)
;;                             '((elpy-company-backend :with company-yasnippet))))))
;;   :custom
;;   ;; (elpy-rpc-backend "jedi") jedi is default already
;;   (elpy-shell-echo-output nil)
;;   (elpy-rpc-python-command "python3")
;;   (elpy-modules (delq 'epy-module-flymake elpy-modules))
;;   :init
;;   (elpy-enable)
;;   )

;; ;; Standard Jedi.el setting
;; (add-hook 'python-mode-hook 'jedi:setup)

;; ;; tell jedi to autocomplete on dot
;; (setq jedi:complete-on-dot t)
;; Elpy:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Org-mode][Org-mode:1]]
;; configure org-mode
(use-package org
  :ensure t
  :config
  (add-hook 'org-mode-hook 'auto-fill-mode)
  (add-hook 'org-mode-hook 'visual-line-mode)
  :pin org)

;; setup the bullet mode
(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;; remove org-indent
(setq org-startup-indented -1)

;; add org-tempo
(require 'org-tempo)

;; org-mode remove emphasis
(setq org-hide-emphasis-markers t)

;; change org-mode lists
(font-lock-add-keywords 'org-mode '(("^,*\\([-]\\) "
                                     (0 (prog1 ()(compose-region (match-beginning 1) (match-end 1) "•"))))))

;; auto detection of bibliography in org-mode
;; (setq org-latex-pdf-process (list
;;                              "latexmk -pdflatex='lualatex -shell-escape -interaction
;;         nonstopmode' -pdf -f %f"))
;; Org-mode:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Org-mode][Org-mode:2]]
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
;; Org-mode:2 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Auto-fill-mode][Auto-fill-mode:1]]
(setq text-mode-hook '(lambda () (auto-fill-mode 1)))
;; Auto-fill-mode:1 ends here

;; [[file:../Projects/ricefields/emacs/.emacs.d/config.org::*Misc packages][Misc packages:1]]
(global-hl-line-mode t)

(use-package beacon 
  :ensure t
  :config
  (beacon-mode 1))
;; Misc packages:1 ends here
